@extends('layout')
@section('content')
<head>
    <title>Productos</title>
</head>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="#">Tienda Laravel</a>
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="{{ __('cart') }}">Carrito</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                </div>
                <div class="card-body">
                    @foreach ($products as $product)
                        <h3>Producto</h3>
                        <li>{{$product->nombre}}</li>
                        <h3>Precio</h3>
                        <li>{{$product->precio}}</li>
                        <img  src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}" width="100"/><br>
                        <a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-primary text-center" role="button">Añadir</a>
                    @endforeach
                </div>
            <a href="{{'cart'}}" class="btn btn-warning text-center" role="button">Ir al carrito</a>
            </div>
        </div>
    </div>
</div>
@endsection
