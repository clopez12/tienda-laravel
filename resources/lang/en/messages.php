<?php

return [
    'Welcome' => 'Welcome',
    'Email' => 'Email',
    'Register' => 'Register',
    'Name' => 'Name',
    'Password' => 'Password',
    'Confirm Password' => 'Confirm password',
    'Surname' => 'Surname',
    'DNI' => 'ID',
    'Already registered?' => 'Already registered?'
];
