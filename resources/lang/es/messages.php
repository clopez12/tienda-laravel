<?php

return [
    'Welcome' => 'Welcome',
    'Email' => 'Correo Electrónico',
    'Register' => 'Regístrate',
    'Name' => 'Nombre',
    'Password' => 'Contraseña',
    'Confirm Password' => 'Confirma la contraseña',
    'Surname' => 'Apellido',
    'DNI' => 'DNI',
    'Already registered?' => 'Ya estás registrado?'
];
