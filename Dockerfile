FROM php:7.4-apache
RUN apt-get update && apt-get install git -y
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
  && apt-get update && apt-get upgrade -y \
  && apt-get install -y git libpq-dev libpng-dev libonig-dev libxml2-dev zip unzip \
  && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
  && docker-php-ext-install pdo pdo_pgsql