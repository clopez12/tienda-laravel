<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Cart_Item;
use App\Models\Product;
use App\Models\Cart;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Request $request)
    {
        $value = empty($request->input('price')) ? '0' : $request->input('price');
        $all = DB::select(DB::raw("select * from products where precio > $value"));
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 2;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $products->setPath($request->url());
        return view('products')->with('products', $products);
    }

    public function addToCart($id)
    {
        $product = Product::find($id);
        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id",$user)->first();

        if ( empty($cart_db) ){
            $cart_db = new Cart();
            $cart_db->user_id = $user;
            $cart_db->save();
        }

        if(!$product) {
            abort(404);
        }
        $cart = session()->get('cart');

        if(!$cart) {
            $cart = [
                $id => [
                    "nombre" => $product->nombre,
                    "cantidad" => 1,
                    "precio" => $product->precio,
                    "image" => $product->image ]
            ];

            $cart_item = new Cart_Item();
            $cart_item->cart_id=$cart_db->id;
            $cart_item->product_id=$id;
            $cart_item->cantidad=1;
            $cart_item->save();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if(isset($cart[$id])) {
            $cart[$id]['cantidad']++;
            $cart_item = Cart_Item::where([
                ['cart_id',$cart_db->id],
                ['product_id',$id],
            ])->first();
            $cart_item->cantidad=$cart[$id]['cantidad'];
            $cart_item->update();
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            "nombre" => $product->nombre,
            "cantidad" => 1,
            "precio" => $product->precio,
            "image" => $product->image
        ];

        $cart_item = new Cart_Item();
        $cart_item->cart_id=$cart_db->id;
        $cart_item->product_id=$id;
        $cart_item->cantidad=1;
        $cart_item->save();

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');

    }

    public function cart()
    {
        return view('cart');
    }

    public function update(Request $request)
    {
        if ($request->id && $request->cantidad) {
            $cart = session()->get('cart');
            $cart[$request->id]["cantidad"] = $request->cantidad;
            $user = Auth::user()->id;
            $cart = Cart::where('user_id', $user)->first();
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if ($request->id) {
            $cart = session()->get('cart');
            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function venta(Request $request)
    {
        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id", $user)->first();
        $cart = session()->get('cart');
        $product_id = array_keys($cart);

        foreach ($product_id as $id) {
            $product = Product::find($id);
            $stock = $product->stock;
            $cantidadPedido = $cart[$id]["cantidad"];
            if ($cantidadPedido > $stock) {
                return redirect()->back()->with('error', 'No hay stock suficiente para ' . $product->nombre . '. Quita alguno y vuelve a probar');
            } else {
                $product->stock = ($stock - $cantidadPedido);
                $product->update();

                DB::table('cart_items')->where('cart_id', '=', $cart_db->id)->delete();

                session()->remove('cart');
            }
            echo "<p class='text-left'>Enhorabuena, se ha hecho el pedido CORRECTAMENTE</p><br>";
            echo "<a class='btn btn-primary' href='/products' role='button'>Volver a productos</a>";
        }
    }

}
