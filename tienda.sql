--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1.pgdg100+1)
-- Dumped by pg_dump version 13.2 (Debian 13.2-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: casos; Type: TABLE; Schema: public; Owner: laravel
--

CREATE TABLE public.casos (
    id bigint NOT NULL,
    fecha date NOT NULL,
    ccaas_id integer NOT NULL,
    numero numeric(8,2) NOT NULL
);


ALTER TABLE public.casos OWNER TO laravel;

--
-- Name: casos_id_seq; Type: SEQUENCE; Schema: public; Owner: laravel
--

CREATE SEQUENCE public.casos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.casos_id_seq OWNER TO laravel;

--
-- Name: casos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: laravel
--

ALTER SEQUENCE public.casos_id_seq OWNED BY public.casos.id;


--
-- Name: ccaas; Type: TABLE; Schema: public; Owner: laravel
--

CREATE TABLE public.ccaas (
    id bigint NOT NULL,
    nombre character varying(255) NOT NULL,
    pais_id integer NOT NULL
);


ALTER TABLE public.ccaas OWNER TO laravel;

--
-- Name: ccaas_id_seq; Type: SEQUENCE; Schema: public; Owner: laravel
--

CREATE SEQUENCE public.ccaas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ccaas_id_seq OWNER TO laravel;

--
-- Name: ccaas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: laravel
--

ALTER SEQUENCE public.ccaas_id_seq OWNED BY public.ccaas.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: laravel
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO laravel;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: laravel
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO laravel;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: laravel
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: ia14; Type: TABLE; Schema: public; Owner: laravel
--

CREATE TABLE public.ia14 (
    id bigint NOT NULL,
    fecha date NOT NULL,
    ccaas_id integer NOT NULL,
    incidencia double precision NOT NULL
);


ALTER TABLE public.ia14 OWNER TO laravel;

--
-- Name: ia14_id_seq; Type: SEQUENCE; Schema: public; Owner: laravel
--

CREATE SEQUENCE public.ia14_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ia14_id_seq OWNER TO laravel;

--
-- Name: ia14_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: laravel
--

ALTER SEQUENCE public.ia14_id_seq OWNED BY public.ia14.id;


--
-- Name: ia7; Type: TABLE; Schema: public; Owner: laravel
--

CREATE TABLE public.ia7 (
    id bigint NOT NULL,
    fecha date NOT NULL,
    ccaas_id integer NOT NULL,
    incidencia numeric(8,2) NOT NULL
);


ALTER TABLE public.ia7 OWNER TO laravel;

--
-- Name: ia7_id_seq; Type: SEQUENCE; Schema: public; Owner: laravel
--

CREATE SEQUENCE public.ia7_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ia7_id_seq OWNER TO laravel;

--
-- Name: ia7_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: laravel
--

ALTER SEQUENCE public.ia7_id_seq OWNED BY public.ia7.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: laravel
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO laravel;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: laravel
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO laravel;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: laravel
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: muertos; Type: TABLE; Schema: public; Owner: laravel
--

CREATE TABLE public.muertos (
    id bigint NOT NULL,
    fecha date NOT NULL,
    ccaas_id integer NOT NULL,
    numero numeric(8,2) NOT NULL
);


ALTER TABLE public.muertos OWNER TO laravel;

--
-- Name: muertos_id_seq; Type: SEQUENCE; Schema: public; Owner: laravel
--

CREATE SEQUENCE public.muertos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.muertos_id_seq OWNER TO laravel;

--
-- Name: muertos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: laravel
--

ALTER SEQUENCE public.muertos_id_seq OWNED BY public.muertos.id;


--
-- Name: paises; Type: TABLE; Schema: public; Owner: laravel
--

CREATE TABLE public.paises (
    id bigint NOT NULL,
    nombre character varying(255) NOT NULL
);


ALTER TABLE public.paises OWNER TO laravel;

--
-- Name: paises_id_seq; Type: SEQUENCE; Schema: public; Owner: laravel
--

CREATE SEQUENCE public.paises_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.paises_id_seq OWNER TO laravel;

--
-- Name: paises_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: laravel
--

ALTER SEQUENCE public.paises_id_seq OWNED BY public.paises.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: laravel
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO laravel;

--
-- Name: users; Type: TABLE; Schema: public; Owner: laravel
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.users OWNER TO laravel;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: laravel
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO laravel;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: laravel
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: casos id; Type: DEFAULT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.casos ALTER COLUMN id SET DEFAULT nextval('public.casos_id_seq'::regclass);


--
-- Name: ccaas id; Type: DEFAULT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.ccaas ALTER COLUMN id SET DEFAULT nextval('public.ccaas_id_seq'::regclass);


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: ia14 id; Type: DEFAULT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.ia14 ALTER COLUMN id SET DEFAULT nextval('public.ia14_id_seq'::regclass);


--
-- Name: ia7 id; Type: DEFAULT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.ia7 ALTER COLUMN id SET DEFAULT nextval('public.ia7_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: muertos id; Type: DEFAULT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.muertos ALTER COLUMN id SET DEFAULT nextval('public.muertos_id_seq'::regclass);


--
-- Name: paises id; Type: DEFAULT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.paises ALTER COLUMN id SET DEFAULT nextval('public.paises_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: casos; Type: TABLE DATA; Schema: public; Owner: laravel
--

COPY public.casos (id, fecha, ccaas_id, numero) FROM stdin;
1	2021-12-03	1	129.00
2	2021-11-03	1	160.00
3	2021-10-03	1	167.00
4	2021-09-03	1	0.00
5	2021-08-03	1	90.00
6	2021-05-03	1	122.00
7	2021-04-03	1	138.00
8	2021-03-03	1	196.00
9	2021-02-03	1	107.00
10	2021-01-03	1	0.00
11	2021-12-02	1	126.00
12	2021-11-02	1	793.00
13	2021-10-02	1	1043.00
14	2021-09-02	1	1381.00
15	2021-08-02	1	371.00
16	2021-05-02	1	1167.00
17	2021-04-02	1	1232.00
18	2021-03-02	1	1488.00
19	2021-02-02	1	1757.00
20	2021-01-02	1	509.00
21	2021-12-01	1	3441.00
22	2021-11-01	1	996.00
23	2021-08-01	1	2943.00
24	2021-07-01	1	794.00
25	2021-05-01	1	3029.00
26	2021-04-01	1	836.00
27	2020-11-12	1	1043.00
28	2020-10-12	1	1137.00
29	2020-09-12	1	257.00
30	2020-07-12	1	206.00
31	2020-04-12	1	693.00
32	2020-03-12	1	744.00
33	2020-02-12	1	788.00
34	2020-01-12	1	914.00
35	2020-12-11	1	221.00
36	2020-11-11	1	1458.00
37	2020-10-11	1	1914.00
38	2020-09-11	1	540.00
39	2020-06-11	1	885.00
40	2021-12-03	1	129.00
41	2021-11-03	1	160.00
42	2021-10-03	1	167.00
43	2021-09-03	1	0.00
44	2021-08-03	1	90.00
45	2021-05-03	1	122.00
46	2021-04-03	1	138.00
47	2021-03-03	1	196.00
48	2021-02-03	1	107.00
49	2021-01-03	1	0.00
50	2021-12-02	1	126.00
51	2021-11-02	1	793.00
52	2021-10-02	1	1043.00
53	2021-09-02	1	1381.00
54	2021-08-02	1	371.00
55	2021-05-02	1	1167.00
56	2021-04-02	1	1232.00
57	2021-03-02	1	1488.00
58	2021-02-02	1	1757.00
59	2021-01-02	1	509.00
60	2021-12-01	1	3441.00
61	2021-11-01	1	996.00
62	2021-08-01	1	2943.00
63	2021-07-01	1	794.00
64	2021-05-01	1	3029.00
65	2021-04-01	1	836.00
66	2020-11-12	1	1043.00
67	2020-10-12	1	1137.00
68	2020-09-12	1	257.00
69	2020-07-12	1	206.00
70	2020-04-12	1	693.00
71	2020-03-12	1	744.00
72	2020-02-12	1	788.00
73	2020-01-12	1	914.00
74	2020-12-11	1	221.00
75	2020-11-11	1	1458.00
76	2020-10-11	1	1914.00
77	2020-09-11	1	540.00
78	2020-06-11	1	885.00
79	2020-05-11	1	1238.00
80	2020-04-11	1	1619.00
81	2020-03-11	1	531.00
82	2020-02-11	1	0.00
83	2020-12-10	1	195.00
84	2020-09-10	1	240.00
85	2020-08-10	1	179.00
86	2020-07-10	1	198.00
87	2020-06-10	1	152.00
88	2020-05-10	1	121.00
89	2020-02-10	1	153.00
90	2020-01-10	1	50.00
91	2020-11-09	1	198.00
92	2020-10-09	1	115.00
93	2020-09-09	1	93.00
94	2020-08-09	1	147.00
95	2020-07-09	1	87.00
96	2020-04-09	1	160.00
97	2020-03-09	1	113.00
98	2020-02-09	1	87.00
99	2020-01-09	1	142.00
100	2021-12-03	1	129.00
101	2021-11-03	1	160.00
102	2021-10-03	1	167.00
103	2021-09-03	1	0.00
104	2021-12-03	1	129.00
105	2021-11-03	1	160.00
106	2021-10-03	1	167.00
\.


--
-- Data for Name: ccaas; Type: TABLE DATA; Schema: public; Owner: laravel
--

COPY public.ccaas (id, nombre, pais_id) FROM stdin;
1	Cataluña	1
\.


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: laravel
--

COPY public.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- Data for Name: ia14; Type: TABLE DATA; Schema: public; Owner: laravel
--

COPY public.ia14 (id, fecha, ccaas_id, incidencia) FROM stdin;
40	2021-12-03	1	166
41	2021-11-03	1	169
42	2021-10-03	1	172
43	2021-09-03	1	185
44	2021-08-03	1	185
45	2021-05-03	1	191
46	2021-04-03	1	196
47	2021-03-03	1	199
48	2021-02-03	1	202
49	2021-01-03	1	207
50	2021-12-02	1	344
51	2021-11-02	1	375
52	2021-10-02	1	391
53	2021-09-02	1	414
54	2021-08-02	1	445
55	2021-05-02	1	462
56	2021-04-02	1	477
57	2021-03-02	1	494
58	2021-02-02	1	519
59	2021-01-02	1	548
60	2021-12-01	1	523
61	2021-11-01	1	524
62	2021-08-01	1	434
63	2021-07-01	1	411
64	2021-05-01	1	373
65	2021-04-01	1	337
66	2020-11-12	1	191
67	2020-10-12	1	186
68	2020-09-12	1	187
69	2020-07-12	1	205
70	2020-04-12	1	208
71	2020-03-12	1	213
72	2020-02-12	1	219
73	2020-01-12	1	233
74	2020-12-11	1	589
75	2020-11-11	1	642
76	2020-10-11	1	678
77	2020-09-11	1	711
78	2020-06-11	1	720
79	2020-05-11	1	742
80	2020-04-11	1	752
81	2020-03-11	1	748
82	2020-02-11	1	726
83	2020-12-10	1	262
84	2020-09-10	1	211
85	2020-08-10	1	194
86	2020-07-10	1	190
87	2020-06-10	1	187
88	2020-05-10	1	191
89	2020-02-10	1	156
90	2020-01-10	1	144
91	2020-11-09	1	168
92	2020-10-09	1	170
93	2020-09-09	1	173
94	2020-08-09	1	180
95	2020-07-09	1	186
96	2020-04-09	1	176
97	2020-03-09	1	175
98	2020-02-09	1	174
99	2020-01-09	1	179
100	2021-12-03	1	166
101	2021-11-03	1	169
102	2021-10-03	1	172
103	2021-09-03	1	185
104	2021-08-03	1	185
105	2021-12-03	1	166
106	2021-11-03	1	169
107	2021-10-03	1	172
\.


--
-- Data for Name: ia7; Type: TABLE DATA; Schema: public; Owner: laravel
--

COPY public.ia7 (id, fecha, ccaas_id, incidencia) FROM stdin;
1	2021-12-03	1	28.00
2	2021-11-03	1	72.00
3	2021-10-03	1	26.00
4	2021-09-03	1	92.00
5	2021-08-03	1	92.00
6	2021-05-03	1	36.00
7	2021-04-03	1	91.00
8	2021-03-03	1	28.00
9	2021-02-03	1	85.00
10	2021-01-03	1	90.00
11	2021-12-02	1	84.00
12	2021-11-02	1	25.00
13	2021-10-02	1	68.00
14	2021-09-02	1	57.00
15	2021-08-02	1	24.00
16	2021-05-02	1	92.00
17	2021-04-02	1	2.00
18	2021-03-02	1	34.00
19	2021-02-02	1	86.00
20	2021-01-02	1	66.00
21	2021-12-01	1	95.00
22	2021-11-01	1	0.00
23	2021-08-01	1	88.00
24	2021-07-01	1	47.00
25	2021-05-01	1	25.00
26	2021-04-01	1	40.00
27	2020-11-12	1	76.00
28	2020-10-12	1	44.00
29	2020-09-12	1	10.00
30	2020-07-12	1	47.00
31	2020-04-12	1	54.00
32	2020-03-12	1	64.00
33	2020-02-12	1	45.00
34	2020-01-12	1	96.00
35	2020-12-11	1	8.00
36	2020-11-11	1	13.00
37	2020-10-11	1	44.00
38	2020-09-11	1	22.00
39	2020-06-11	1	20.00
40	2021-12-03	1	28.00
41	2021-11-03	1	72.00
42	2021-10-03	1	26.00
43	2021-09-03	1	92.00
44	2021-08-03	1	92.00
45	2021-05-03	1	36.00
46	2021-04-03	1	91.00
47	2021-03-03	1	28.00
48	2021-02-03	1	85.00
49	2021-01-03	1	90.00
50	2021-12-02	1	84.00
51	2021-11-02	1	25.00
52	2021-10-02	1	68.00
53	2021-09-02	1	57.00
54	2021-08-02	1	24.00
55	2021-05-02	1	92.00
56	2021-04-02	1	2.00
57	2021-03-02	1	34.00
58	2021-02-02	1	86.00
59	2021-01-02	1	66.00
60	2021-12-01	1	95.00
61	2021-11-01	1	0.00
62	2021-08-01	1	88.00
63	2021-07-01	1	47.00
64	2021-05-01	1	25.00
65	2021-04-01	1	40.00
66	2020-11-12	1	76.00
67	2020-10-12	1	44.00
68	2020-09-12	1	10.00
69	2020-07-12	1	47.00
70	2020-04-12	1	54.00
71	2020-03-12	1	64.00
72	2020-02-12	1	45.00
73	2020-01-12	1	96.00
74	2020-12-11	1	8.00
75	2020-11-11	1	13.00
76	2020-10-11	1	44.00
77	2020-09-11	1	22.00
78	2020-06-11	1	20.00
79	2020-05-11	1	42.00
80	2020-04-11	1	54.00
81	2020-03-11	1	83.00
82	2020-02-11	1	70.00
83	2020-12-10	1	98.00
84	2020-09-10	1	80.00
85	2020-08-10	1	68.00
86	2020-07-10	1	51.00
87	2020-06-10	1	16.00
88	2020-05-10	1	96.00
89	2020-02-10	1	58.00
90	2020-01-10	1	80.00
91	2020-11-09	1	99.00
92	2020-10-09	1	5.00
93	2020-09-09	1	48.00
94	2020-08-09	1	76.00
95	2020-07-09	1	39.00
96	2020-04-09	1	15.00
97	2020-03-09	1	20.00
98	2020-02-09	1	30.00
99	2020-01-09	1	64.00
100	2021-12-03	1	28.00
101	2021-11-03	1	72.00
102	2021-10-03	1	26.00
103	2021-09-03	1	92.00
104	2021-08-03	1	92.00
105	2021-12-03	1	28.00
106	2021-11-03	1	72.00
107	2021-10-03	1	26.00
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: laravel
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_08_19_000000_create_failed_jobs_table	1
4	2021_03_15_150923_paises	1
5	2021_03_15_150954_ccaas	1
6	2021_03_15_151000_ia14	1
7	2021_03_15_151004_ia7	1
8	2021_03_15_151009_casos	1
9	2021_03_15_151023_muertos	1
\.


--
-- Data for Name: muertos; Type: TABLE DATA; Schema: public; Owner: laravel
--

COPY public.muertos (id, fecha, ccaas_id, numero) FROM stdin;
40	2021-12-03	1	53.00
41	2021-11-03	1	46.00
42	2021-10-03	1	34.00
43	2021-09-03	1	0.00
44	2021-08-03	1	91.00
45	2021-05-03	1	468.00
46	2021-04-03	1	78.00
47	2021-03-03	1	169.00
48	2021-02-03	1	-78.00
49	2021-01-03	1	155.00
50	2021-12-02	1	87.00
51	2021-11-02	1	55.00
52	2021-10-02	1	64.00
53	2021-09-02	1	48.00
54	2021-08-02	1	101.00
55	2021-05-02	1	29.00
56	2021-04-02	1	37.00
57	2021-03-02	1	15.00
58	2021-02-02	1	19.00
59	2021-01-02	1	19.00
60	2021-12-01	1	27.00
61	2021-11-01	1	35.00
62	2021-08-01	1	31.00
63	2021-07-01	1	22.00
64	2021-05-01	1	42.00
65	2021-04-01	1	10.00
66	2020-11-12	1	39.00
67	2020-10-12	1	8284.00
68	2020-09-12	1	0.00
69	2020-07-12	1	0.00
70	2020-04-12	1	24.00
71	2020-03-12	1	8095.00
72	2020-02-12	1	-7996.00
73	2020-01-12	1	92.00
74	2020-12-11	1	29.00
75	2020-11-11	1	12.00
76	2020-10-11	1	27.00
77	2020-09-11	1	47.00
78	2020-06-11	1	78.00
79	2020-05-11	1	74.00
80	2020-04-11	1	1037.00
81	2020-03-11	1	35.00
82	2020-02-11	1	10.00
83	2020-12-10	1	8.00
84	2020-09-10	1	10.00
85	2020-08-10	1	4.00
86	2020-07-10	1	3.00
87	2020-06-10	1	5.00
88	2020-05-10	1	2.00
89	2020-02-10	1	4.00
90	2020-01-10	1	9.00
91	2020-11-09	1	5.00
92	2020-10-09	1	-1.00
93	2020-09-09	1	-2.00
94	2020-08-09	1	-2.00
95	2020-07-09	1	0.00
96	2020-04-09	1	-1.00
97	2020-03-09	1	0.00
98	2020-02-09	1	6.00
99	2020-01-09	1	0.00
100	2021-12-03	1	53.00
101	2021-11-03	1	46.00
102	2021-10-03	1	34.00
103	2021-09-03	1	0.00
104	2021-08-03	1	91.00
105	2021-12-03	1	53.00
106	2021-11-03	1	46.00
107	2021-10-03	1	34.00
\.


--
-- Data for Name: paises; Type: TABLE DATA; Schema: public; Owner: laravel
--

COPY public.paises (id, nombre) FROM stdin;
1	España
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: laravel
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: laravel
--

COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
\.


--
-- Name: casos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laravel
--

SELECT pg_catalog.setval('public.casos_id_seq', 106, true);


--
-- Name: ccaas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laravel
--

SELECT pg_catalog.setval('public.ccaas_id_seq', 1, true);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laravel
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Name: ia14_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laravel
--

SELECT pg_catalog.setval('public.ia14_id_seq', 107, true);


--
-- Name: ia7_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laravel
--

SELECT pg_catalog.setval('public.ia7_id_seq', 107, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laravel
--

SELECT pg_catalog.setval('public.migrations_id_seq', 9, true);


--
-- Name: muertos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laravel
--

SELECT pg_catalog.setval('public.muertos_id_seq', 107, true);


--
-- Name: paises_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laravel
--

SELECT pg_catalog.setval('public.paises_id_seq', 1, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: laravel
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


--
-- Name: casos casos_pkey; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.casos
    ADD CONSTRAINT casos_pkey PRIMARY KEY (id);


--
-- Name: ccaas ccaas_nombre_unique; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.ccaas
    ADD CONSTRAINT ccaas_nombre_unique UNIQUE (nombre);


--
-- Name: ccaas ccaas_pkey; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.ccaas
    ADD CONSTRAINT ccaas_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);


--
-- Name: ia14 ia14_pkey; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.ia14
    ADD CONSTRAINT ia14_pkey PRIMARY KEY (id);


--
-- Name: ia7 ia7_pkey; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.ia7
    ADD CONSTRAINT ia7_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: muertos muertos_pkey; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.muertos
    ADD CONSTRAINT muertos_pkey PRIMARY KEY (id);


--
-- Name: paises paises_nombre_unique; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.paises
    ADD CONSTRAINT paises_nombre_unique UNIQUE (nombre);


--
-- Name: paises paises_pkey; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.paises
    ADD CONSTRAINT paises_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: laravel
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: ccaas ccaas_pais_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.ccaas
    ADD CONSTRAINT ccaas_pais_id_foreign FOREIGN KEY (pais_id) REFERENCES public.paises(id) ON DELETE CASCADE;


--
-- Name: ia14 ia14_ccaas_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.ia14
    ADD CONSTRAINT ia14_ccaas_id_foreign FOREIGN KEY (ccaas_id) REFERENCES public.ccaas(id) ON DELETE CASCADE;


--
-- Name: muertos muertos_ccaas_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: laravel
--

ALTER TABLE ONLY public.muertos
    ADD CONSTRAINT muertos_ccaas_id_foreign FOREIGN KEY (ccaas_id) REFERENCES public.ccaas(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

