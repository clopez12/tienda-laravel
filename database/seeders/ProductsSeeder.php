<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'nombre' => 'Mario',
            'precio' => 12.5,
            'stock' => 5,
            'descripcion' => 'its me mario',
            'image' => base64_encode(file_get_contents('/home/itb/Desktop/mario.png')),
        ]);
    }
}
