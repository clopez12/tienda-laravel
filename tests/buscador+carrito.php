Route::get('/products', function(){
$productos = App\Product::where('precio','>','1')->paginate(10);
return view('products')->with('productos',$productos);
});
```


Creamos el controlador
```bash
php artisan make:controller ProductController
```

Redefinimos nuestra ruta para que apunte al controlador -routes/web.php-
```bash
Route::get('/products', [ProductController::class,'show'])->name('products');
```

Para que solo se muestre a los usuarios autenticados añadimos lo siguiente al constructor:
```bash
public function __construct()
{
$this->middleware('auth');
}
```
Ahora podemos escribir el método show, que capture el formulario y devuelva los datos pedidos.

```bash
public function mostrar(Request $request){
$value = empty($request->input('price'))?'0':$request->input('price') ;

$all = DB::select(DB::raw("select * from products where precio > $value"));

$currentPage = LengthAwarePaginator::resolveCurrentPage();
$itemCollection = collect($all);
$perPage = 2;
$currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
$productos= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
$productos->setPath($request->url());

return view('products')->with('products',$products);
}
```

Ahora añadimos un nuevo campo en la vistaAra volem que la vista tingui un filtre pel preu

Afegim a la vista de productes
```bash
<div class="card-header">
    <form method="get" action="/products">
        <input name="price" type="text"/>
        <input value="Busca" type="submit"/>
    </form>
</div>
```

Nos falta añadir los enlaces a las distintas paginaciones.
```sh
{{ $products->links() }}
```
# Carrito de la compra

## Laravel carrito de la compra

Añadimos a nuestra vista de productos un botón para añadir al carrito

```bash
<p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">añadir</a> </p>
```

Añadimos al fichero de rutas la ruta nueva y la que nos mostrará el carrito
```bash
Route::get('add-to-cart/{id}', [ProductController::class,'addToCart']);
Route::get('cart', [ProductController::class,'cart']);

```

Ahora en el controllador debemos añadir el método, addtoCart

```bash
public function addToCart($id){
$product = Product::find($id);
if(!$product) {
abort(404);
}
$cart = session()->get('cart');

if(!$cart) {
$cart = [
$id => [
"nombre" => $product->nombre,
"cantidad" => 1,
"precio" => $product->precio,
"image" => $product->image
]
];

session()->put('cart', $cart);
return redirect()->back()->with('success', 'Product added to cart successfully!');
}

if(isset($cart[$id])) {
$cart[$id]['cantidad']++;
session()->put('cart', $cart);
return redirect()->back()->with('success', 'Product added to cart successfully!');
}

$cart[$id] = [
"nombre" => $product->nombre,
"cantidad" => 1,
"precio" => $product->precio,
"image" => $product->image
];

session()->put('cart', $cart);
return redirect()->back()->with('success', 'Product added to cart successfully!');
}

```

Creamos la vista para el carrito cart.blade.php y layout.blade.php

layout.blade.php
```bash
<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>@yield('title')</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"&gt;
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"&gt;


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"&gt;&lt;/script&gt;
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"&gt;&lt;/script&gt;
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"&gt;&lt;/script&gt;

</head>
<body>


<div class="container page">
    @yield('content')
</div>


@yield('scripts')

</body>
</html>
```

cart.blade.php
```bash
@extends('layout')

@section('title', 'Cart')

@section('content')

@if(session('success'))

<div class="alert alert-success">
    {{ session('success') }}
</div>

@endif

<table id="cart" class="table table-hover table-condensed">
    <thead>
    <tr>
        <th style="width:50%">Producto</th>
        <th style="width:10%">Precio</th>
        <th style="width:8%">Cantidad</th>
        <th style="width:22%" class="text-center">Subtotal</th>
        <th style="width:10%"></th>
    </tr>
    </thead>
    <tbody>

    <?php $total = 0 ?>

    @if(session('cart'))

    @foreach(session('cart') as $id => $details)

    <?php $total += $details['precio'] * $details['cantidad'];
    $products = App\Models\Product::find($id);

    ?>

    <tr>
        <td >
            <div class="row">
                <div class="col-sm-3">
                    {{-- <img src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>--}}
                    <img src="data:image/jpeg;base64,{!! stream_get_contents($products->image) !!}" width="100" height="100" class="img-responsive"/>
                </div>
                <div class="col-sm-9">
                    <h4 class="nomargin">{{ $details['nombre'] }}</h4>
                </div>
            </div>
        </td>
        <td data-th="Price">{{ $details['precio'] }}</td>
        <td data-th="Quantity">
            <input type="number" value="{{ $details['cantidad'] }}" class="form-control quantity" />
        </td>
        <td data-th="Subtotal" class="text-center">{{ $details['precio'] * $details['cantidad'] }}</td>
        <td class="actions" data-th="">
            <button class="update-cart" data-id="{{ $id }}"><i class="fa fa-refresh"></i></button>
            <button class="remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-o"></i></button>
        </td>
    </tr>
    @endforeach
    @endif

    </tbody>
    <tfoot>

    <tr>
        <td><a href="{{ url('/products') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
        <td colspan="2" class="hidden-xs"></td>
        <td class="hidden-xs text-center"><strong>Total {{ $total }}€</strong></td>
    </tr>
    </tfoot>
</table>

@endsection


@section('scripts')


<script type="text/javascript">

    $(".update-cart").click(function (e) {
        e.preventDefault();

        var ele = $(this);

        $.ajax({
            url: '{{ url('update-cart') }}',
            method: "patch",
            data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), cantidad: ele.parents("tr").find(".quantity").val()},
            success: function (response) {
                window.location.reload();
            }
        });
    });

    $(".remove-from-cart").click(function (e) {
        e.preventDefault();

        var ele = $(this);

        if(confirm("Are you sure")) {
            $.ajax({
                url: '{{ url('remove-from-cart') }}',
                method: "DELETE",
                data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                success: function (response) {
                    window.location.reload();
                }
            });
        }
    });

</script>

@endsection


```

Por último debemos añadir las rutas de update y remove en web.php
```bash
Route::patch('update-cart',[ProductController::class,'update']);
Route::delete('remove-from-cart',[ProductController::class,'remove']);
```

Y añadir los métodos al controlador

```bash
public function cart()
{
return view('cart');
}

public function update(Request $request){
if($request->id and $request->cantidad){
$cart = session()->get('cart');
$cart[$request->id]["cantidad"] = $request->cantidad;
session()->put('cart', $cart);
session()->flash('success', 'Cart updated successfully');
}
}

public function remove(Request $request){
if($request->id) {
$cart = session()->get('cart');
if(isset($cart[$request->id])) {
unset($cart[$request->id]);
session()->put('cart', $cart);
}
session()->flash('success', 'Product removed successfully');
}
}
